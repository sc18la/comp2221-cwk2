import java.io.*;
import java.net.*;
import java.util.*;

public class Client
{

	public void connect(String[] args) {

			String host = "localhost";

			//A connection is attempted to be made with the localhost server on port
			//7777
			try{
				Socket socket = new Socket(host, 7777);

				//Protocol: Writer to output to the server
				PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

				//Protocol: Buffer the input stream for performance.
				BufferedReader in = new BufferedReader(
												new InputStreamReader(
														socket.getInputStream()));

//if there's only 1 argument is must be 'show' if not then an error message is displayed
					String input = args[0];
					if(input.toLowerCase().equals("show")){
						//The "show" command is sent to the server
						out.println(input);
						String poll;
						while((poll = in.readLine()) != null){
							System.out.println(poll);
						}
						in.close();
						System.exit(0);

				}
//if there are 2 arguments then the program checks if it is a vote
				if(args.length==2){
					String option = args[1];
						//Vote command with the option is sent to the server
						out.println(args[0] + " " + args[1]);
						//out.close();
						String response = in.readLine();
						System.out.println(response);
						in.close();
						System.exit(0);
				}
			}
			//If the hostname does not exist an exception is thrown
			catch (UnknownHostException ex) {
        System.err.println(ex);
      }

			catch (IOException ex) {
        System.err.println("No server is present at this socket");
      }
	}


	public static void main( String[] args )
	{
		Client client = new Client();
		//Arguments from command line is passed to the connect method

		//if there's only 1 argument is must be 'show' if not then an error message is displayed
						if(args.length==1){
							String input = args[0];
							if(input.toLowerCase().equals("show")){
								client.connect(args);
							}
							else{
								System.out.println("Invalid argument submitted");
								System.exit(0);
							}
						}
		//if there are 2 arguments then the program checks if it is a vote
						else if(args.length==2){
							String input = args[0];
							if(input.toLowerCase().equals("vote")){
								client.connect(args);
							}
							else{
								System.out.println("Invalid arguments submitted");
								System.exit(0);
							}
					}
	}
}
