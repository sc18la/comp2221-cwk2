import java.net.*;
import java.io.*;
import java.util.concurrent.*;
import java.util.Arrays;
import java.lang.Thread;

public class Server
{

	public static void main( String[] args )
	{
		//When server is initialised all scores in the poll are set to 0
		int[] scores = new int[args.length];
		Arrays.fill(scores,0);

		Server server = new Server();
		if(args.length > 1){
			try{
				ServerSocket serverSock = new ServerSocket(7777);
				ExecutorService service = null ;
				ExecutorService executor = Executors.newFixedThreadPool(20);
				//Set up log text file
				try {
	        File logFile = new File("log.txt");
					if(!logFile.exists()){
						logFile.createNewFile();
					}
					else{
						logFile.delete();
						logFile.createNewFile();
					}
	      } catch (IOException e) {
	        System.out.println("An error occurred while creating the log file");
	        e.printStackTrace();
	      }

				//Client handler object is initiated
				while(true){
					new ClientHandler(serverSock.accept(), args, scores).start();
				}
			}
			catch (IOException ex) {
					System.out.println( ex );
			}
		}
		else{
			System.out.println("Too few options submitted");
			System.exit(0);
		}
	}
}
