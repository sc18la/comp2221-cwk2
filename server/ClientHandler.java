import java.net.*;
import java.io.*;
import java.util.concurrent.*;
import java.util.Arrays;
import java.lang.Thread;
import java.time.*;

public class ClientHandler extends Thread{
  private Socket sock = null;
  private String[] options = null;
  private int[] scores = null;

//Constructor
  public ClientHandler(Socket sock, String[] options, int[] scores){
    super("ClientHandler");
    this.sock = sock;
    this.options = options;
    this.scores = scores;
  }

  public void run(){

	try{
	//Protocol: Send a line of text
	PrintWriter out = new PrintWriter(sock.getOutputStream(), true);

	//Protocol: Read a line of text
	BufferedReader in = new BufferedReader(
									new InputStreamReader(
											sock.getInputStream()));

	//Information from the client is read below and dealt with accordingly
		String command = in.readLine();
		if(command != null){
		//Show command
		if(command.equals("show")){
			for(int i = 0; i < options.length; i++){
				out.println(options[i] + " has " + scores[i] + " vote(s)");
			}
			out.close();
		}

		//Vote Command
		else{
			String[] voteOption = command.split(" ");
			if(Arrays.asList(options).contains(voteOption[1])){
				int index = Arrays.asList(options).indexOf(voteOption[1]);
				scores[index] = scores[index] + 1;
				out.println("The new total for " + voteOption[1] +
																			" is " + scores[index]);
                                      out.close();
			}
			else{out.println("Voting option submitted does not exist");
           out.close();}
			}

      //Logging
      try {
        File logFile = new File("log.txt");
        FileWriter fileWriter = new FileWriter("log.txt",true);
        BufferedWriter bw = new BufferedWriter(fileWriter);
        InetAddress inet = sock.getInetAddress();
        LocalDate date =  LocalDate.now();
        LocalTime time = LocalTime.now();

        //Formatted log is outputted to the text file
        bw.write(date.toString() +":"+ time.toString()
                         +":"+ inet.toString() +":"+ command);
        bw.newLine();
        bw.close();

      } catch (IOException e) {
        System.out.println("An error occurred while logging");
        e.printStackTrace();
      }
		}



	}
		catch (IOException ex) {
				System.out.println( ex );
		}
	}
}
